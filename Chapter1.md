# Introduction

## What is an Operating System

A computer system consist of
    - hardware
    - system program
    - application program

## What does an Operating System do

1. It is an extended machine
2. It is a resource manager

## History of Operating System

1. First generation 1945 - 1955
    - vacuum tubes
    - plug boards
2. Second generation 1955 - 1965
    - transistor
    - batch systems
3. Third generation 1965 - 1980
    - ICs
    - multiprogramming
4. Fourth generation 1980 - present
    - personal computers
        + DOS
        + MS-DOS
        + windows
            * Windows 95
            * Windows 98
            * + Windows Me
            * Windows NT
            * Windows 2000
            * Windows XP
            * Windows Server
            * Windows Vista
            * Windows 7
            * Windows 8
        + MacOS X
        + UNIX
            * freeBSD
5. Present - next 5 - 10 years
    - mobile device
        + Symbian OS
        + Blackberry OS
        + Android
        + iOS
        + Windows Phone
    - many-core computers





















