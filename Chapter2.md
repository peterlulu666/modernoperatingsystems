# Computer and Operating Systems

## Computer Hardware Review

1. CPU: data processing
2. Memory: volatile data storage
3. I/O: monitor, keyboard, USB printer, hard disk drive
4. Bus: the CPU, memory, and I/O are all connected by a system bus and communicate with one another over it

### CPU

1. Components
    - Arithmetic Logic Unit
    - Control Unit
2. CLock rate
    - The speed at which a CPU is running
3. Data storage
    - General-purpose registers
        + hold variables and temporary results
    - Special-purpose registers
        + program counter contains the memory address of the next instruction to be fetched
        + stack pointer points to the top of the current stack in memory
        + program status word 
4. Parallelism
    - Instruction-level parallelism
    - Thread-level parallelism
        + Hyperthreading allows the CPU to hold the state of two different threads and then switch back and forth on a nanosecond time scale      
        + Replicated      

### Memory

1. Register
2. Cache memory
    - A larger size than registers
    - A much faster speed than memory
    - Concurrent accesses to memory when cache misses occur

### Disk

A stack of platters, a surface with a magnetic coating

### I/O

1. Device controller
    - The controller is a chip or a set of chips that physically controls the device
    - It accepts commands from the operating system
    - It provides a simple interface of device control to OS
2. Device driver
    - The software that talks to the controller, giving it commands and accepting responses
3. I/O notifies the OS
    - Polling
        + a user program issues a system call, which the kernel then translates into a procedure call to the appropriate driver. The driver then starts the I/O and sits in a tight loop continuously polling the device to see if it is done
        + the I/O device put information in a status register
        + the OS periodically check the status register

    - Interrupt
        + Whenever an I/O device needs attention from the processor, it interrupts the processor from    what it is currently doing      
        + driver starts the device and asks it to give an interrupt when it is finished      
    
    - DMA
        + Delegate I/O responsibility from CPU
        + a DMA chip controls the flow of bits between memory and some controller without CPU intervention

## Interrupt      

1. Interrupt
    - An    interruption    of  the normal  sequence    of  execution
    - Improves  processing  efficiency
    - Allows    the processor   to  execute other   instructions    while   an  I/O operation   is  in progress      
    - A suspension  of  a   process caused  by  an  event   external    to  that    process and performed   in  such    a   way that    the process can be  resumed
2. Types    of  interrupts
    - I/O
        + CPU    writes  cmds in to  device  registers
        + The  device  signals interrupt    controller
        + Interrupt    controller  informs a CPU   
        + The  CPU accepts the interrupt   and triggers    the service routine
    - Program
    - Timer, Hardware failure
























